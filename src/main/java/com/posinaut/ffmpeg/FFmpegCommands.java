package com.posinaut.ffmpeg;

import android.content.Context;
import com.posinaut.ffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.posinaut.ffmpeg.util.CommandUtil;
import com.posinaut.ffmpeg.workers.IFFmpegExecuteResponseHandler;
import com.posinaut.ffmpeg.workers.IFFmpegTranscodeResponseHandler;
import com.posinaut.media.ImageParams;

public class FFmpegCommands {

    public static final int STATUS_FFMPEG_TRANSCODE_SUCCEEDED = 1000;
    public static final int STATUS_FFMPEG_TRANSCODE_FAILED = 1010;

    public static void concat(Context context,
                              String inputFilesList,
                              String outputFileName,
                              IFFmpegExecuteResponseHandler handler) throws FFmpegCommandAlreadyRunningException {

        StringBuilder sb = new StringBuilder();
        sb.append("-f concat -i ").append(inputFilesList);
        sb.append(" -c copy ");
        sb.append(outputFileName);

        FFmpeg.getInstance(context).execute(FFmpegExecutable.FFMPEG, sb.toString(), handler);
    }

    /**
     * Generate a single thumbnail from the video at the given frame time.
     *
     * @param context
     * @param inputFileName
     * @param frameTime
     * @param params
     * @param outputFileName
     * @param handler
     * @throws FFmpegCommandAlreadyRunningException
     */
    public static void generateThumbnail(Context context,
                                         String inputFileName,
                                         long frameTime,
                                         ImageParams params,
                                         String outputFileName,
                                         IFFmpegExecuteResponseHandler handler) throws FFmpegCommandAlreadyRunningException {

        StringBuilder sb = new StringBuilder();
        sb.append("-i ").append(inputFileName);
        sb.append(" -ss ").append(CommandUtil.timeToString(frameTime));
        if (params != null) {
            sb.append(" -s ").append(params.getWidth()).append("x").append(params.getHeight());
        }
        sb.append(" -vframes 1 ");
        sb.append(outputFileName);

        FFmpeg.getInstance(context).execute(FFmpegExecutable.FFMPEG, sb.toString(), handler);
    }

    /**
     * Generate multiple thumbnails from the video starting at the given frame time.
     * <p/>
     * Notes (framesPerSecond):
     * To generate 1 frame every second, set framesPerSecond = 1.
     * To generate 1 frame every minute, set framesPerSecond = 1/60.
     * To generate 1 frame every 10 minutes, set framesPerSecond = 1/600.
     * <p/>
     * Notes (outputFileTemplate):
     * Template for absolute path to output image. Eg. /sdcard/thumb%04d.jpg generates
     * /sdcard/thumb0000.jpg, /sdcard/thumb0001.jpg, /sdcard/thumb0002.jpg, etc.
     * <p/>
     * References:
     * https://trac.ffmpeg.org/wiki/Create%20a%20thumbnail%20image%20every%20X%20seconds%20of%20the%20video
     * http://www.digitalwhores.net/ffmpeg/ffmpeg-image-thumbnail-every-minute-seconds-the-math/
     *
     * @param context
     * @param inputFileName
     * @param startFrameTime
     * @param framesPerSecond
     * @param params
     * @param outputFileTemplate
     * @param handler
     * @throws FFmpegCommandAlreadyRunningException
     */
    public static void generateThumbnails(Context context,
                                          String inputFileName,
                                          long startFrameTime,
                                          float framesPerSecond,
                                          ImageParams params,
                                          String outputFileTemplate,
                                          IFFmpegExecuteResponseHandler handler) throws FFmpegCommandAlreadyRunningException {

        StringBuilder sb = new StringBuilder();
        sb.append("-i ").append(inputFileName);
        sb.append(" -ss ").append(CommandUtil.timeToString(startFrameTime));
        if (params != null) {
            sb.append(" -s ").append(params.getWidth()).append("x").append(params.getHeight());
        }
        sb.append(" -vf ").append(framesPerSecond);
        sb.append(outputFileTemplate);

        FFmpeg.getInstance(context).execute(FFmpegExecutable.FFMPEG, sb.toString(), handler);
    }

    public static void transcode(Context context,
                                 TranscodingParams params,
                                 IFFmpegTranscodeResponseHandler handler) throws FFmpegCommandAlreadyRunningException {
        StringBuilder sb = new StringBuilder();
        sb.append("-y -i ").append(params.getInputFileName());
        sb.append(" -ss ").append(CommandUtil.timeToString(params.getStartTime()));
        sb.append(" -t ").append(CommandUtil.timeToString(params.getEndTime()));
        sb.append(" -s  ").append(params.getOutputVideoWidth()).append("x").append(params.getOutputVideoHeight());
        sb.append(" -vcodec mpeg4 ");
        sb.append(" -qscale 12 ");
        sb.append(" -acodec copy ");
        sb.append(params.getOutputFileName());

        FFmpeg.getInstance(context).execute(FFmpegExecutable.FFMPEG, sb.toString(), handler);
    }

    public static void ffprobe(Context context,
                               String inputFile,
                               IFFmpegExecuteResponseHandler handler) throws FFmpegCommandAlreadyRunningException {

        StringBuilder sb = new StringBuilder();
        sb.append(" -v quiet -print_format json -show_format -show_streams ").append(inputFile);

        FFmpeg.getInstance(context).execute(FFmpegExecutable.FFPROBE, sb.toString(), handler);
    }

}
