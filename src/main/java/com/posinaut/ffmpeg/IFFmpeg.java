package com.posinaut.ffmpeg;

import com.posinaut.ffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.posinaut.ffmpeg.exceptions.FFmpegNotSupportedException;
import com.posinaut.ffmpeg.workers.IFFmpegExecuteResponseHandler;
import com.posinaut.ffmpeg.workers.IFFmpegLoadBinaryResponseHandler;

import java.util.Map;

public interface IFFmpeg {
    /**
     * Loads the FFmpeg binaries to the device according to architecture. This also updates the binaries if the binaries on device are an older version.
     *
     * @param ffmpegLoadBinaryResponseHandler {@link IFFmpegLoadBinaryResponseHandler}
     * @throws FFmpegNotSupportedException {@link FFmpegNotSupportedException}
     */
    public void loadBinaries(IFFmpegLoadBinaryResponseHandler ffmpegLoadBinaryResponseHandler) throws FFmpegNotSupportedException;

    /**
     * Executes a command
     *
     * @param executable The FFmpeg binary to execute
     * @param environvenmentVars Environment variables
     * @param cmd command to execute
     * @param ffmpegExecuteResponseHandler {@link IFFmpegExecuteResponseHandler}
     * @throws FFmpegCommandAlreadyRunningException {@link FFmpegCommandAlreadyRunningException}
     */
    public void execute(FFmpegExecutable executable, Map<String, String> environvenmentVars, String cmd, IFFmpegExecuteResponseHandler ffmpegExecuteResponseHandler) throws FFmpegCommandAlreadyRunningException;

    /**
     * Executes a command
     *
     * @param executable The FFmpeg binary to execute
     * @param cmd command to execute
     * @param ffmpegExecuteResponseHandler {@link IFFmpegExecuteResponseHandler}
     * @throws FFmpegCommandAlreadyRunningException {@link FFmpegCommandAlreadyRunningException}
     */
    public void execute(FFmpegExecutable executable, String cmd, IFFmpegExecuteResponseHandler ffmpegExecuteResponseHandler) throws FFmpegCommandAlreadyRunningException;

    /**
     * Gets the version of the executable currently on the device.
     *
     * @param executable The executable to get the version info of.
     * @return version info from 'ffmpeg -version', 'ffprobe -version', etc.
     * @throws FFmpegCommandAlreadyRunningException
     */
    public String getDeviceBinaryVersion(FFmpegExecutable executable) throws FFmpegCommandAlreadyRunningException;

    /**
     * Checks if an FFmpeg binary is currently running a command
     *
     * @param executable The executable to check
     * @return true if the binary is running a command
     */
    public boolean isFFmpegBinaryCommandRunning(FFmpegExecutable executable);


    /**
     * Kill any processes that might be running.
     * <p/>
     * This includes the following processes:
     * 1. Copy process (copies apk binaries to device).
     * 2. FFmpeg proces
     * 3. FFprobe process.
     */
    public void killRunningProcesses();

    /**
     * Timeout for FFmpeg executable process.
     * <p/>
     * Should be a minimum of 10 seconds.
     *
     * @param executable The executable on which to set the timeout.
     * @param timeout    in milliseconds.
     */
    public void setTimeout(FFmpegExecutable executable, long timeout);

}
