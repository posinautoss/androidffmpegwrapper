package com.posinaut.ffmpeg;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.posinaut.cpu.CpuArchHelper;
import com.posinaut.ffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.posinaut.ffmpeg.exceptions.FFmpegNotSupportedException;
import com.posinaut.ffmpeg.util.ExecutableUtil;
import com.posinaut.ffmpeg.util.Util;
import com.posinaut.ffmpeg.workers.CommandResult;
import com.posinaut.ffmpeg.workers.FFmpegExecuteAsyncTask;
import com.posinaut.ffmpeg.workers.FFmpegLoadLibraryAsyncTask;
import com.posinaut.ffmpeg.workers.FFprobeExecuteAsyncTask;
import com.posinaut.ffmpeg.workers.IFFmpegExecuteResponseHandler;
import com.posinaut.ffmpeg.workers.IFFmpegLoadBinaryResponseHandler;
import com.posinaut.ffmpeg.workers.ShellCommand;

import java.util.HashMap;
import java.util.Map;

public class FFmpeg implements IFFmpeg {
    private static final String Tag = FFmpeg.class.getSimpleName();

    private static FFmpeg instance = null;

    private static final long MINIMUM_TIMEOUT = 10 * 1000;
    private static final long DEFAULT_TIMEOUT = 5 * 60 * 1000;
    private Map<FFmpegExecutable, Long> timeouts;

    private final Context context;
    private FFmpegLoadLibraryAsyncTask ffmpegLoadLibraryAsyncTask;
    private FFmpegExecuteAsyncTask ffmpegExecuteAsyncTask;
    private FFprobeExecuteAsyncTask ffprobeExecuteAsyncTask;

    private FFmpeg(Context context) {
        this.context = context.getApplicationContext();
        timeouts = new HashMap<FFmpegExecutable, Long>();
        timeouts.put(FFmpegExecutable.FFMPEG, DEFAULT_TIMEOUT);
        timeouts.put(FFmpegExecutable.FFPROBE, DEFAULT_TIMEOUT);
    }

    public static FFmpeg getInstance(Context context) {
        if (instance == null) {
            instance = new FFmpeg(context);
        }
        return instance;
    }

    @Override
    public void loadBinaries(IFFmpegLoadBinaryResponseHandler ffmpegLoadBinaryResponseHandler) throws FFmpegNotSupportedException {
        String platformAssetsFolder = null;
        switch (CpuArchHelper.getCpuArch()) {
            case x86:
                Log.i(Tag, "Loading FFmpeg for x86 CPU");
                platformAssetsFolder = Constants.FOLDER_NAME_PLATFORM_x86;
                break;
            case ARMv7:
                Log.i(Tag, "Loading FFmpeg for armv7 CPU");
                platformAssetsFolder = Constants.FOLDER_NAME_PLATFORM_ARM_V7;
                break;
            case ARMv7_NEON:
                Log.i(Tag, "Loading FFmpeg for armv7-neon CPU");
                platformAssetsFolder = Constants.FOLDER_NAME_PLATFORM_ARM_V7_NEON;
                break;
            case NONE:
                throw new FFmpegNotSupportedException("Device not supported");
        }

        if (!TextUtils.isEmpty(platformAssetsFolder)) {
            ffmpegLoadLibraryAsyncTask = new FFmpegLoadLibraryAsyncTask(context, platformAssetsFolder, ffmpegLoadBinaryResponseHandler);
            ffmpegLoadLibraryAsyncTask.execute();
        }
    }

    @Override
    public void execute(FFmpegExecutable executable, Map<String, String> environvenmentVars, String cmd, IFFmpegExecuteResponseHandler ffmpegExecuteResponseHandler)
            throws FFmpegCommandAlreadyRunningException {

        if (isFFmpegBinaryCommandRunning(executable)) {
            throw new IllegalArgumentException(executable + " is already executing a command. You can only run single command at a time.");
        } else if (TextUtils.isEmpty(cmd)) {
            throw new IllegalArgumentException("command cannot be empty.");
        }

        String binaryCmd = ExecutableUtil.getDeviceBinary(context, executable, environvenmentVars) + " " + cmd;
        long timeout = timeouts.get(executable);

        switch (executable) {
            case FFMPEG:
                ffmpegExecuteAsyncTask = new FFmpegExecuteAsyncTask(binaryCmd, timeout, ffmpegExecuteResponseHandler);
                ffmpegExecuteAsyncTask.execute();
                break;

            case FFPROBE:
                ffprobeExecuteAsyncTask = new FFprobeExecuteAsyncTask(binaryCmd, timeout, ffmpegExecuteResponseHandler);
                ffprobeExecuteAsyncTask.execute();
                break;
        }

    }

    @Override
    public void execute(FFmpegExecutable executable, String cmd, IFFmpegExecuteResponseHandler ffmpegExecuteResponseHandler) throws FFmpegCommandAlreadyRunningException {
        execute(executable, null, cmd, ffmpegExecuteResponseHandler);
    }

    @Override
    public String getDeviceBinaryVersion(FFmpegExecutable executable) throws FFmpegCommandAlreadyRunningException {
        String binaryFilePath = ExecutableUtil.getDeviceBinary(context, executable);
        ShellCommand shellCommand = new ShellCommand();
        CommandResult commandResult = shellCommand.runWaitFor(binaryFilePath + " -version");
        if (commandResult.isSuccess()) {
            return commandResult.getOutput();
        } else {
            return "Unable to obtain version of: " + binaryFilePath;
        }
    }

    @Override
    public boolean isFFmpegBinaryCommandRunning(FFmpegExecutable executable) {
        switch (executable) {
            case FFMPEG:
                return ffmpegExecuteAsyncTask != null && !ffmpegExecuteAsyncTask.isProcessCompleted();

            case FFPROBE:
                return ffprobeExecuteAsyncTask != null && !ffprobeExecuteAsyncTask.isProcessCompleted();
        }

        return false;
    }

    @Override
    public void killRunningProcesses() {
        Util.killAsync(ffmpegLoadLibraryAsyncTask);
        Util.killAsync(ffmpegExecuteAsyncTask);
        Util.killAsync(ffprobeExecuteAsyncTask);
    }

    @Override
    public void setTimeout(FFmpegExecutable executable, long timeout) {
        if (timeout >= MINIMUM_TIMEOUT) {
            timeouts.put(executable, timeout);
        }
    }

}
