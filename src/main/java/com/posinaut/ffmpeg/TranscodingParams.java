package com.posinaut.ffmpeg;

public class TranscodingParams {
    private String inputFileName;
    private String outputFileName;
    private long startTime;
    private long endTime;
    private int outputVideoWidth;
    private int outputVideoHeight;
    private TransposeType transpose;

    public TranscodingParams(String inputFileName, String outputFileName, long startTime, long endTime, int outputVideoWidth, int outputVideoHeight, TransposeType transpose) {
        this.inputFileName = inputFileName;
        this.outputFileName = outputFileName;
        this.startTime = startTime;
        this.endTime = endTime;
        this.outputVideoWidth = outputVideoWidth;
        this.outputVideoHeight = outputVideoHeight;
        this.transpose = transpose;
    }

    public TranscodingParams(String inputFileName, String outputFileName, long startTime, long endTime, int outputVideoWidth, int outputVideoHeight) {
        this(inputFileName, outputFileName, startTime, endTime, outputVideoWidth, outputVideoHeight, TransposeType.None);
    }

    public void setOutputDimensions(int outputVideoWidth, int outputVideoHeight) {
        this.outputVideoWidth = outputVideoWidth;
        this.outputVideoHeight = outputVideoHeight;
    }

    public String getInputFileName() {
        return inputFileName;
    }

    public String getOutputFileName() {
        return outputFileName;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public int getOutputVideoWidth() {
        return outputVideoWidth;
    }

    public void setOutputVideoWidth(int outputVideoWidth) {
        this.outputVideoWidth = outputVideoWidth;
    }

    public int getOutputVideoHeight() {
        return outputVideoHeight;
    }

    public void setOutputVideoHeight(int outputVideoHeight) {
        this.outputVideoHeight = outputVideoHeight;
    }

    public TransposeType getTranspose() {
        return transpose;
    }

    public void setTranspose(TransposeType transpose) {
        this.transpose = transpose;
    }
}

