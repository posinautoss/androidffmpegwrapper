package com.posinaut.ffmpeg;

public class Constants {
    // The names of the folders for each platform in the assets folder.
    public static final String FOLDER_NAME_PLATFORM_x86 = "x86";
    public static final String FOLDER_NAME_PLATFORM_ARM_V7 = "armeabi-v7a";
    public static final String FOLDER_NAME_PLATFORM_ARM_V7_NEON = "armeabi-v7a-neon";

    // The names of the FFmpeg binaries in the assets folder.
    public static final String FILE_NAME_FFMPEG = "ffmpeg";
    public static final String FILE_NAME_FFPROBE = "ffprobe";
}
