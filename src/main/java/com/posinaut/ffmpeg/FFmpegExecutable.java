package com.posinaut.ffmpeg;

public enum FFmpegExecutable {
    FFMPEG, FFPROBE
}
