package com.posinaut.ffmpeg.workers;

import android.os.AsyncTask;
import android.util.Log;

import com.posinaut.ffmpeg.util.Util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeoutException;

public class FFmpegExecuteAsyncTask extends AsyncTask<Void, String, CommandResult> {
    private static final String Tag = FFmpegExecuteAsyncTask.class.getSimpleName();

    private final String command;
    private final long timeout;
    private final IFFmpegExecuteResponseHandler ffmpegExecuteResponseHandler;
    private final ShellCommand shellCommand;

    private long startTime = Long.MAX_VALUE;
    private Process process = null;
    private String output = null;

    public FFmpegExecuteAsyncTask(String command, long timeout, IFFmpegExecuteResponseHandler ffmpegExecuteResponseHandler) {
        this.command = command;
        this.timeout = timeout <= 0 ? 1000 : timeout;
        this.ffmpegExecuteResponseHandler = ffmpegExecuteResponseHandler;
        this.shellCommand = new ShellCommand();
    }

    @Override
    protected void onPreExecute() {
        startTime = System.currentTimeMillis();
        if (ffmpegExecuteResponseHandler != null) {
            ffmpegExecuteResponseHandler.onStart();
        }
    }

    @Override
    protected CommandResult doInBackground(Void... params) {
        try {
            process = shellCommand.run(command);
            if (process == null) {
                return CommandResult.getDummyFailureResponse();
            }
            Log.d(Tag, "Running FFmpeg cmd: " + command);
            checkAndUpdateProcess();
            return CommandResult.getOutputFromProcess(command, process);
        } catch (TimeoutException e) {
            Log.e(Tag, "FFmpeg command: " + command + "timed out after: " + timeout + "ms.", e);
            return new CommandResult(command, false, e.getMessage());
        } catch (Exception e) {
            Log.e(Tag, "Error running FFmpeg.", e);
        } finally {
            Util.destroyProcess(process);
        }
        return CommandResult.getDummyFailureResponse();
    }

    @Override
    protected void onProgressUpdate(String... values) {
        if (values != null && values[0] != null && ffmpegExecuteResponseHandler != null) {
            ffmpegExecuteResponseHandler.onProgress(values[0]);
        }
    }

    @Override
    protected void onPostExecute(CommandResult commandResult) {
        if (ffmpegExecuteResponseHandler != null) {
            output = output == null ? commandResult.output : output + " " + commandResult.output;
            if (commandResult.success) {
                ffmpegExecuteResponseHandler.onSuccess(output);
            } else {
                ffmpegExecuteResponseHandler.onFailure(output);
            }
            ffmpegExecuteResponseHandler.onFinish();
        }
    }

    private void checkAndUpdateProcess() throws TimeoutException, InterruptedException {
        //todo: refactor below to re-use line, BufferedReader, InputStreamReader, etc.
        // try ... finally?

        while (!Util.isProcessCompleted(process)) {

            // Checking if process is complete...
            if (Util.isProcessCompleted(process)) {
                return;
            }

            // Handling timeout...
            if (timeout != Long.MAX_VALUE && System.currentTimeMillis() > startTime + timeout) {
                throw new TimeoutException("FFmpeg timed out.");
            }

            try {
                String line;
                BufferedReader reader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                while ((line = reader.readLine()) != null) {
                    if (isCancelled()) {
                        return;
                    }

                    output = output == null ? line + "\n" : output + line + "\n";
                    publishProgress(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            Thread.sleep(1000);
        }
    }

    public boolean isProcessCompleted() {
        return Util.isProcessCompleted(process);
    }

}
