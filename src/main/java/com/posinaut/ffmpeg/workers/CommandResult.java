package com.posinaut.ffmpeg.workers;

import com.posinaut.ffmpeg.util.Util;

public class CommandResult {
    protected final String command;
    // Contains the process' output stream if exit code is success, the error stream if exit code is a fail.
    protected final String output;
    protected final boolean success;

    CommandResult(String command, boolean success, String output) {
        this.command = command;
        this.success = success;
        this.output = output;
    }

    static CommandResult getDummyFailureResponse() {
        return new CommandResult(null, false, "");
    }

    static CommandResult getOutputFromProcess(String command, Process process) {
        boolean processExitSuccessful = success(process.exitValue());
        String output = processExitSuccessful ? Util.convertInputStreamToString(process.getInputStream()) :
                Util.convertInputStreamToString(process.getErrorStream());

        return new CommandResult(command, processExitSuccessful, output);
    }

    static boolean success(Integer exitValue) {
        return exitValue != null && exitValue == 0;
    }

    public String getCommand() {
        return command;
    }

    public String getOutput() {
        return output;
    }

    public boolean isSuccess() {
        return success;
    }
}
