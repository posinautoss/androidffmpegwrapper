package com.posinaut.ffmpeg.workers;

abstract interface IResponseHandler {
    public void onStart();

    public void onFinish();

}
