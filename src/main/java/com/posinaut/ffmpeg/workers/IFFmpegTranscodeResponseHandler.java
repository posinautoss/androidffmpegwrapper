package com.posinaut.ffmpeg.workers;

public interface IFFmpegTranscodeResponseHandler extends IFFmpegExecuteResponseHandler {
    public void onTranscodingProgress(float percentComplete);

    public void onTranscodingComplete(int transcodeStatus, String transcodeMessage, String newVideoPath);
}
