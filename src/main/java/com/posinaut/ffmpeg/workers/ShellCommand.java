package com.posinaut.ffmpeg.workers;

import android.util.Log;

import com.posinaut.ffmpeg.util.Util;

import java.io.IOException;

public class ShellCommand {
    private static final String Tag = ShellCommand.class.getSimpleName();

    /**
     * Asynchronously executes a command in a separate process.
     *
     * @param command
     * @return The process if it was successfully started, else null.
     */
    Process run(String command) {
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(command);
        } catch (IOException e) {
            Log.e(Tag, "Exception while trying to run: " + command, e);
        }
        return process;
    }

    /**
     * Synchronously executes a command in a separate process.
     * <p/>
     * Call blocks until the process completes.
     *
     * @param command
     * @return Type containing the result of execution.
     */
    public CommandResult runWaitFor(String command) {
        Process process = run(command);

        Integer exitValue = null;
        String output = null;
        boolean processExitSuccessful = false;

        try {
            if (process != null) {
                exitValue = process.waitFor();
                processExitSuccessful = CommandResult.success(exitValue);
                output = processExitSuccessful ? Util.convertInputStreamToString(process.getInputStream()) :
                        Util.convertInputStreamToString(process.getErrorStream());
            }
        } catch (InterruptedException e) {
            Log.e(Tag, "Interrupt exception", e);
        } finally {
            Util.destroyProcess(process);
            return new CommandResult(command, processExitSuccessful, output);
        }
    }
}
