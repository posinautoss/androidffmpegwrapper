package com.posinaut.ffmpeg.workers;

public interface IFFmpegExecuteResponseHandler extends IResponseHandler {

    /**
     * onProgress
     *
     * @param message current output of FFmpeg command
     */
    public void onProgress(String message);

    /**
     * onFailure
     *
     * @param message complete output of the FFmpeg command
     */
    public void onFailure(String message);

    /**
     * onSuccess
     *
     * @param message complete output of the FFmpeg command
     */
    public void onSuccess(String message);

}