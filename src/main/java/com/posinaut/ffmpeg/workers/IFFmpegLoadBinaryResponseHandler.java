package com.posinaut.ffmpeg.workers;

public interface IFFmpegLoadBinaryResponseHandler extends IResponseHandler {
    public void onFailure(String message);

    public void onSuccess(String message);
}
