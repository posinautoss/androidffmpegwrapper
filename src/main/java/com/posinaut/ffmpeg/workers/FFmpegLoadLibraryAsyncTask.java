package com.posinaut.ffmpeg.workers;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.posinaut.ffmpeg.FFmpegExecutable;
import com.posinaut.ffmpeg.util.ExecutableUtil;

import java.io.File;

public class FFmpegLoadLibraryAsyncTask extends AsyncTask<Void, Void, Boolean> {
    private static final String Tag = FFmpegLoadLibraryAsyncTask.class.getSimpleName();

    private final Context context;
    private final String platformAssetsFolder;
    private final IFFmpegLoadBinaryResponseHandler responseHandler;
    private StringBuilder resultInfo = new StringBuilder();

    public FFmpegLoadLibraryAsyncTask(Context context, String platformAssetsFolder, IFFmpegLoadBinaryResponseHandler responseHandler) {
        this.context = context;
        this.platformAssetsFolder = platformAssetsFolder;
        this.responseHandler = responseHandler;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        resultInfo.append(Tag).append(" > Loading FFmpeg binaries...");
        boolean copyFFmpeg = copyBinaryToDevice(context, platformAssetsFolder, FFmpegExecutable.FFMPEG);
        if (!copyFFmpeg) {
            resultInfo.append("\nLoading FFmpeg failed.");
            return false;
        } else {
            resultInfo.append("\nLoading FFmpeg succeeded.");
            boolean copyFFprobe = copyBinaryToDevice(context, platformAssetsFolder, FFmpegExecutable.FFPROBE);
            if (!copyFFprobe) {
                resultInfo.append("\nLoading FFprobe failed.");
                return false;
            } else {
                resultInfo.append("\nLoading FFprobe succeeded.");
                return true;
            }
        }
    }

    @Override
    protected void onPostExecute(Boolean isSuccess) {
        if (responseHandler != null) {
            if (isSuccess) {
                responseHandler.onSuccess(resultInfo.toString());
            } else {
                responseHandler.onFailure(resultInfo.toString());
            }
            responseHandler.onFinish();
        }
    }

    private boolean isDeviceFFmpegBinaryOld() {
        // todo: what is best approach to determine if the existing binary on the device is older that what's in the apk?
        //return CpuArch.fromString(FileUtils.SHA1(ExecutableUtil.getFFmpeg(context))).equals(CpuArch.NONE);

        // For now it's alway an older version!
        return true;
    }

    private boolean copyBinaryToDevice(Context context, String platformAssetsFolder, FFmpegExecutable executable) {

        StringBuilder sb = new StringBuilder("Checking ");

        try {
            String binaryName = ExecutableUtil.getDeviceBinary(context, executable);
            File binaryFile = new File(binaryName);

            sb.append(binaryName).append("...");

            // If the (installed) binary exists and is an old version...
            if (binaryFile.exists() && isDeviceFFmpegBinaryOld()) {
                sb.append("existing binary is old...");

                // Try to delete it...
                boolean successfulDeletion = ExecutableUtil.deleteBinaryFromData(context, executable);
                if (successfulDeletion) {
                    sb.append("deletion succeeded...");
                } else {
                    sb.append("deletion failed!!!");
                    return false;
                }
            }

            // If the installed binary does not exist (or it was an older version we deleted)...
            if (!binaryFile.exists()) {

                // Copy the binary from the apk's assets folder to the app's data directory...
                boolean isFileCopied = ExecutableUtil.copyBinaryFromAssetsToData(context,
                        platformAssetsFolder, executable);

                // Make the binary executable if it isn't...
                if (isFileCopied) {
                    sb.append("copying succeeded...");
                    if (!binaryFile.canExecute()) {
                        sb.append("not executable, so trying to make it executable...");
                        if (binaryFile.setExecutable(true)) {
                            sb.append("success.");
                            return true;
                        } else {
                            sb.append("failure!!!");
                            return false;
                        }
                    } else {
                        sb.append("binary is executable.");
                        return true;
                    }
                } else {
                    sb.append("copying failed!!!");
                }
            }
            return binaryFile.exists() && binaryFile.canExecute();
        } finally {
            Log.d(Tag, sb.toString());
        }
    }
}

