package com.posinaut.ffmpeg.workers;

import android.text.TextUtils;
import com.posinaut.ffmpeg.FFmpegCommands;
import com.posinaut.ffmpeg.TranscodingParams;
import com.posinaut.ffmpeg.util.CommandUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class FFmpegTranscodeResponseHandler implements IFFmpegTranscodeResponseHandler {
    private static final Pattern patternFrameTime = Pattern.compile("(?<=time=)\\d{2}:\\d{2}:\\d{2}.\\d{1,3}");

    private TranscodingParams params;
    private float percentComplete = 0f;

    public FFmpegTranscodeResponseHandler(TranscodingParams params) {
        this.params = params;
    }

    @Override
    public void onProgress(String message) {
        long endTime = params.getEndTime();

        // We measure progress from the console output. The progress can be determined from the
        //   frame info which displays the time in the video that the operation has progressed to.
        //
        // Transcoding is complete when the final "video:..." line is displayed.
        //
        // Sample console output looks like this...
        //
        // frame=  197 fps= 15 q=12.0 size=     483kB time=00:00:07.00 bitrate= 565.2kbits/s
        // frame=  207 fps= 16 q=12.0 size=     503kB time=00:00:07.00 bitrate= 588.5kbits/s
        // frame=  217 fps= 16 q=12.0 size=     507kB time=00:00:07.27 bitrate= 570.7kbits/s
        // frame=  217 fps= 15 q=12.0 Lsize=     536kB time=00:00:07.28 bitrate= 603.0kbits/s
        // video:443kB audio:85kB subtitle:0kB other streams:0kB global headers:0kB muxing overhead: 1.437242%
        //
        // More info:
        // See http://stackoverflow.com/questions/867383/how-to-link-the-ffmpeg-transcoding-process-information-into-a-vb6-gui-app?lq=1
        //
        if (params != null && !TextUtils.isEmpty(message)) {
            if (message.startsWith("frame=")) {
                Matcher matcher = patternFrameTime.matcher(message);
                if (matcher.find()) {
                    String timeAsString = matcher.group();
                    long currentTranscodeTime = CommandUtil.timeFromString(timeAsString);
                    percentComplete = (currentTranscodeTime * 100) / endTime;
                    onTranscodingProgress(Math.min(percentComplete, 100));
                }
            } else if (message.startsWith("video:") && percentComplete > 0) {
                percentComplete = 100;
                onTranscodingProgress(percentComplete);
            }
        } else if (percentComplete <= 0) {
            onTranscodingProgress(0f);
        }
    }

    @Override
    public void onFailure(String message) {
        onTranscodingComplete(FFmpegCommands.STATUS_FFMPEG_TRANSCODE_FAILED, message, params.getOutputFileName());
    }

    @Override
    public void onSuccess(String message) {
        if (percentComplete >= 100) {
            onTranscodingComplete(FFmpegCommands.STATUS_FFMPEG_TRANSCODE_SUCCEEDED, null, params.getOutputFileName());
        }
    }

    @Override
    public void onStart() {
    }

    @Override
    public void onFinish() {
    }
}


