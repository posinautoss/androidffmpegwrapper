package com.posinaut.ffmpeg;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class VideoData {
    private static final String TAG_STREAMS = "streams";
    private static final String TAG_TAGS = "tags";
    private static final String TAG_CODEC_TYPE = "codec_type";
    private static final String TAG_STREAM_VIDEO = "video";
    private static final String TAG_STREAM_AUDIO = "audio";
    private static final String TAG_VIDEO_WIDTH = "width";
    private static final String TAG_VIDEO_HEIGHT = "height";
    private static final String TAG_VIDEO_DURATION = "duration";
    private static final String TAG_VIDEO_ROTATION = "rotate";

    protected Integer videoWidth;
    protected Integer videoHeight;
    protected Integer videoDuration;
    protected Integer videoRotation;

    public VideoData(Integer width, Integer height, Integer duration, Integer rotation) {
        videoWidth = width;
        videoHeight = height;
        videoDuration = duration;
        videoRotation = rotation;
    }

    public Integer getVideoWidth() {
        return videoWidth;
    }

    public Integer getVideoHeight() {
        return videoHeight;
    }

    public Integer getVideoDuration() {
        return videoDuration;
    }

    public Integer getVideoRotation() {
        return videoRotation;
    }

    /**
     * Extracts video/audio data from FFprobe output.
     *
     * @param jsonStr The JSON encoded output from running FFprobe on a given input.
     * @return
     * @throws JSONException
     */
    public static VideoData parse(String jsonStr) throws JSONException {
        VideoData result = null;
        JSONObject containerMeta = new JSONObject(jsonStr);
        JSONObject videoMeta = null;
        JSONObject audioMeta = null;

        JSONArray streams = containerMeta.getJSONArray(TAG_STREAMS);
        for (int i = 0; i < streams.length(); i++) {
            JSONObject stream = streams.getJSONObject(i);
            String videoType = stream.getString(TAG_CODEC_TYPE);
            if (TAG_STREAM_VIDEO.equals(videoType)) {
                videoMeta = stream;
            } else if (TAG_STREAM_AUDIO.equals(videoType)) {
                audioMeta = stream;
            }
        }

        if (videoMeta != null) {
            Integer w = getIntValue(videoMeta, TAG_VIDEO_WIDTH);
            Integer h = getIntValue(videoMeta, TAG_VIDEO_HEIGHT);
            Integer d = (int) (getDoubleValue(videoMeta, TAG_VIDEO_DURATION) * 1000); // Convert to milliseconds.

            JSONObject tags = videoMeta.getJSONObject(TAG_TAGS);
            Integer r = getIntValue(tags, TAG_VIDEO_ROTATION);

            result = new VideoData(w, h, d, r);
        }

        return result;
    }

    private static Integer getIntValue(JSONObject json, String key) {
        Integer result = null;

        if (json != null) {
            try {
                result = json.getInt(key);
            } catch (JSONException e) {
                // No op. Possible for key not to exist.
            }
        }
        return result;
    }

    private static Double getDoubleValue(JSONObject json, String key) {
        Double result = null;

        if (json != null) {
            try {
                result = json.getDouble(key);
            } catch (JSONException e) {
                // No op. Possible for key not to exist.
            }
        }
        return result;
    }

    private static String getStringValue(JSONObject json, String key) {
        String result = null;

        if (json != null) {
            try {
                result = json.getString(key);
            } catch (JSONException e) {
                // No op. Possible for key not to exist.
            }
        }
        return result;
    }

}

