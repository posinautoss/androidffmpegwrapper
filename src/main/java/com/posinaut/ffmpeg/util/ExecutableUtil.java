package com.posinaut.ffmpeg.util;

import android.content.Context;
import com.posinaut.ffmpeg.Constants;
import com.posinaut.ffmpeg.FFmpegExecutable;

import java.io.File;
import java.util.Map;

public class ExecutableUtil {
    private static final String Tag = ExecutableUtil.class.getSimpleName();

    private static File getFilesDirectory(Context context) {
        // Should be /data/data/package_name
        return context.getFilesDir();
    }

    public static String getDeviceBinary(Context context, FFmpegExecutable executable) {
        switch (executable) {
            case FFMPEG:
                return getFilesDirectory(context).getAbsolutePath() + File.separator + Constants.FILE_NAME_FFMPEG;
            case FFPROBE:
                return getFilesDirectory(context).getAbsolutePath() + File.separator + Constants.FILE_NAME_FFPROBE;
            default:
                throw new IllegalArgumentException("Unknown executable value.");
        }
    }

    public static String getDeviceBinary(Context context, FFmpegExecutable executable, Map<String, String> environmentVars) {
        String ffmpegCommand = "";
        if (environmentVars != null) {
            for (Map.Entry<String, String> var : environmentVars.entrySet()) {
                ffmpegCommand += var.getKey() + "=" + var.getValue() + " ";
            }
        }
        ffmpegCommand += getDeviceBinary(context, executable);
        return ffmpegCommand;
    }

    public static boolean copyBinaryFromAssetsToData(Context context, String platformAssetsFolder, FFmpegExecutable executable) {
        switch (executable) {
            case FFMPEG:
                return copyBinaryFromAssetsToData(context, platformAssetsFolder, Constants.FILE_NAME_FFMPEG, Constants.FILE_NAME_FFMPEG);
            case FFPROBE:
                return copyBinaryFromAssetsToData(context, platformAssetsFolder, Constants.FILE_NAME_FFPROBE, Constants.FILE_NAME_FFPROBE);
            default:
                throw new IllegalArgumentException("Unknown executable value.");
        }
    }

    private static boolean copyBinaryFromAssetsToData(Context context, String platformAssetsFolder, String fileNameFromAssets, String outputFileName) {
        // FFmpeg binaries will be copied to /data/data/package_name
        File outputDirectory = getFilesDirectory(context);
        return FileUtil.copyFromAssets(context, platformAssetsFolder, fileNameFromAssets, outputDirectory, outputFileName);
    }

    public static boolean deleteBinaryFromData(Context context, FFmpegExecutable executable) {
        switch (executable) {
            case FFMPEG:
                return deleteBinaryFromData(context, Constants.FILE_NAME_FFMPEG);
            case FFPROBE:
                return deleteBinaryFromData(context, Constants.FILE_NAME_FFPROBE);
            default:
                throw new IllegalArgumentException("Unknown executable value.");
        }
    }

    private static boolean deleteBinaryFromData(Context context, String fileName) {
        File dataDirectory = getFilesDirectory(context);

        File file = new File(dataDirectory, fileName);
        if (file.exists()) {
            return file.delete();
        } else {
            return false;
        }
    }

}
