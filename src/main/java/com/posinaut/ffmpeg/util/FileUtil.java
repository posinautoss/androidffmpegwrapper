package com.posinaut.ffmpeg.util;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;

import java.io.*;

public class FileUtil {
    public static final int DEFAULT_BUFFER_SIZE = 1024 * 4;
    public static final int EOF = -1;

    private static final String BASE_DIR = Environment.getExternalStorageDirectory().getAbsolutePath();
    private static String FILE_HIDER = ".nomedia";

    /**
     * Creates a folder in external storage.
     *
     * @param folderName  Folder to create.
     * @param withNoMedia true to create a .nomedia marker file inside.
     * @return
     */
    public static boolean createExternalStorageFolder(String folderName, boolean withNoMedia) {
        if (TextUtils.isEmpty(folderName)) return false;

        FileOutputStream fos = null;

        try {
            File folder = new File(BASE_DIR + File.separator + folderName);
            if (!(folder.exists())) {
                folder.mkdirs();
            }

            File noMediaFile = new File(folder, FILE_HIDER);
            if (withNoMedia && !noMediaFile.exists()) {
                fos = new FileOutputStream(noMediaFile);
            } else if (!withNoMedia && noMediaFile.exists()) {
                noMediaFile.delete();
            }

            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } finally {
            Util.close(fos);
        }
    }

    /**
     * Copies a file from the app's assets folder.
     *
     * @param sourceFolder   Source folder inside assets.
     * @param sourceFileName Source file.
     * @param targetFolder
     * @param targetFileName
     * @return
     */
    public static boolean copyFromAssets(Context context, String sourceFolder, String sourceFileName, String targetFolder, String targetFileName) {
        return copyFromAssets(context, sourceFolder, sourceFileName, new File(targetFolder), targetFileName);
    }

    public static boolean copyFromAssets(Context context, String sourceFolder, String sourceFileName, File targetFolder, String targetFileName) {
        InputStream is = null;
        FileOutputStream os = null;

        try {

            is = context.getAssets().open(TextUtils.isEmpty(sourceFolder) ? sourceFileName : sourceFolder + File.separator + sourceFileName);
            os = new FileOutputStream(new File(targetFolder, targetFileName));
            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];

            int n;
            while (EOF != (n = is.read(buffer))) {
                os.write(buffer, 0, n);
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            Util.close(os);
            Util.close(is);
        }
    }
}