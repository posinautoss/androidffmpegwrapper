package com.posinaut.ffmpeg.util;

import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandUtil {
    static final Pattern patternTime = Pattern.compile("\\d{2}:\\d{2}:\\d{2}.\\d{1,3}");
    static final Pattern patternHMS = Pattern.compile("\\d{1,3}");

    /**
     * Converts a millisecond time to a formatted string: hh:mm:ss.xxx where xxx in [000,999] for milliseconds.
     *
     * @param time
     * @return
     */
    public static String timeToString(long time) {
        long hours = time / (60 * 60 * 1000);
        long mins = (time / (60 * 1000)) % 60;
        long secs = (time / 1000) % 60;
        long millisecs = time % 1000;

        return String.format("%02d:%02d:%02d.%03d", hours, mins, secs, millisecs);
    }

    /**
     * Converts a formatted string: hh:mm:ss.xxx (where xxx in [000,999] for milliseconds) to millisecond time.
     *
     * @param time
     * @return
     */
    public static long timeFromString(String time) {
        if (TextUtils.isEmpty(time)) {
            throw new IllegalArgumentException("Expected string in time format: hh:mm:ss.xx");
        }

        Matcher matchOverall = patternTime.matcher(time);

        if (!matchOverall.matches()) {
            throw new IllegalArgumentException("Expected string in time format: hh:mm:ss.xx");
        } else {
            int hour = 0, min = 0, sec = 0, milli = 0;

            Matcher matchHMS = patternHMS.matcher(time);
            while (matchHMS.find()) {
                int startIndex = matchHMS.start();
                if (startIndex == 0) {
                    hour = Integer.parseInt(matchHMS.group());
                } else if (startIndex == 3) {
                    min = Integer.parseInt(matchHMS.group());
                } else if (startIndex == 6) {
                    sec = Integer.parseInt(matchHMS.group());
                } else if (startIndex == 9) {
                    milli = Integer.parseInt(matchHMS.group());
                }
            }

            return (hour * 60 * 60 * 1000) + (min * 60 * 1000) + (sec * 1000) + milli;
        }
    }
}

