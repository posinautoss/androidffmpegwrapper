package com.posinaut.ffmpeg;

public enum TransposeType {
    None,
    Clockwise90,
    Clockwise90VerticalFlip,
    CounterClockwise90,
    CounterClockwise90VerticalFlip,
    Rotate180
}

