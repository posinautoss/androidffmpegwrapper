package com.posinaut.media;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.MediaMetadataRetriever;
import android.text.TextUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class GrabImageViaSdk implements IGrabImage {
    private static boolean notSupportGrabImage = false;

    protected Context context;

    public GrabImageViaSdk(Context ctx) {
        context = ctx;
    }

    @Override
    public void grabImage(IGrabImageListener listener, String videofilePath, int frameTimeAt, ImageParams imgParams, String imagePath) {
        if (notSupportGrabImage) {
            notifyImageCompletion(listener, IGrabImageListener.STATUS_IMAGE_GRAB_SDK_NOT_SUPPORTED, null, null);
            return;
        }

        MediaMetadataRetriever mRetriever = new MediaMetadataRetriever();
        try {
            mRetriever.setDataSource(videofilePath);
            long timeUs = TimeUnit.MILLISECONDS.toMicros(frameTimeAt);
            Bitmap frameImage = mRetriever.getFrameAtTime(timeUs);

            if (frameImage != null) {
                File imageFile = new File(imagePath);
                FileOutputStream imageOut = new FileOutputStream(imageFile);

                frameImage.compress(Bitmap.CompressFormat.JPEG, 85, imageOut);
                imageOut.flush();
                imageOut.close();

                String[] arr = {imagePath};
                notifyImageCompletion(listener, IGrabImageListener.STATUS_IMAGE_GRAB_COMPLETED, null, arr);
            } else {
                notifyImageCompletion(listener, IGrabImageListener.STATUS_IMAGE_GRAB_SDK_FAILED, null, null);
            }
        } catch (IOException e) {
            e.printStackTrace();
            notifyImageCompletion(listener, IGrabImageListener.STATUS_IMAGE_GRAB_EXCEPTION, e.getMessage(), null);
        } finally {
            mRetriever.release();
        }
    }

    @Override
    public void grabImages(IGrabImageListener listener, String videofilePath, int fromPoint, int everyMilliseconds,
                           int imageCount, ImageParams imgParams, String imageFolder, String imgFileNameFormat) {
        if (notSupportGrabImage) {
            notifyImageCompletion(listener, IGrabImageListener.STATUS_IMAGE_GRAB_SDK_NOT_SUPPORTED, null, null);
            return;
        }

        List<String> outputImages = new ArrayList<String>();
        MediaMetadataRetriever mRetriever = new MediaMetadataRetriever();
        try {
            mRetriever.setDataSource(videofilePath);
            long timeUs = TimeUnit.MILLISECONDS.toMicros(fromPoint);

            if (TextUtils.isEmpty(imgFileNameFormat)) {
                imgFileNameFormat = IGrabImage.THUMBNAIL_FILENAME_FORMAT;
            }

            long firstImageSize = 0;
            int imageNo = 0;

            while (imageNo < imageCount) {
                Bitmap frameImage = mRetriever.getFrameAtTime(timeUs);
                String imgPath = imageFolder + File.separator + String.format(imgFileNameFormat, imageNo);
                File imageFile = new File(imgPath);

                if (imageFile.exists()) {
                    imageFile.delete();
                }

                if (!imageFile.createNewFile()) {
                    throw new IOException("Unable to create image file.");
                }

                // Resize if needed.
                if (imgParams.getWidth() > 0 && imgParams.getHeight() > 0) {
                    final int bitmapWidth = frameImage.getWidth();
                    final int bitmapHeight = frameImage.getHeight();
                    Matrix matrix = new Matrix();
                    matrix.postScale(imgParams.getWidth() * 1f / bitmapWidth, imgParams.getHeight() * 1f / bitmapHeight);
                    frameImage = Bitmap.createBitmap(frameImage, 0, 0, bitmapWidth, bitmapHeight, matrix, false);
                }

                FileOutputStream imageOut = new FileOutputStream(imageFile);
                frameImage.compress(Bitmap.CompressFormat.JPEG, 85, imageOut);

                imageOut.flush();
                imageOut.close();
                outputImages.add(imgPath);

                if (imageNo == 0) {
                    firstImageSize = imageFile.length();
                } else if (imageNo == 1) {
                    // On some devices, the same image is created all the time regardless of frame time selected.
                    if (firstImageSize == imageFile.length()) {
                        notSupportGrabImage = true;
                        notifyImageCompletion(listener, IGrabImageListener.STATUS_IMAGE_GRAB_SDK_NOT_SUPPORTED, null, null);
                        return;
                    }
                }

                notifyImageProgress(listener, imageNo, imgPath);
                imageNo++;
                timeUs += TimeUnit.MILLISECONDS.toMicros(everyMilliseconds);
            }

            notifyImageCompletion(listener, IGrabImageListener.STATUS_IMAGE_GRAB_COMPLETED, null, outputImages.toArray(new String[outputImages.size()]));

        } catch (IOException e) {
            e.printStackTrace();
            notifyImageCompletion(listener, IGrabImageListener.STATUS_IMAGE_GRAB_EXCEPTION, e.getMessage(), null);
        } finally {
            mRetriever.release();
        }
    }

    private void notifyImageProgress(IGrabImageListener listener, int imageNo, String imagePath) {
        if (listener != null) {
            listener.onGrabImageProgress(imageNo, imagePath);
        }
    }

    private void notifyImageCompletion(IGrabImageListener listener, int statusCode, String statusMessage, String[] imagesPath) {
        if (listener != null) {
            listener.onGrabImageComplete(statusCode, statusMessage, imagesPath);
        }
    }
}


