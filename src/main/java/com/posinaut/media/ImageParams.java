package com.posinaut.media;

public class ImageParams {
    private int width;
    private int height;

    public ImageParams(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
