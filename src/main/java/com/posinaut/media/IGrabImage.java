package com.posinaut.media;

public interface IGrabImage {
    public static final String THUMBNAIL_FILENAME_FORMAT = "thumbnail_%04d.jpg";

    public void grabImage(IGrabImageListener listener, String videofilePath, int frameTimeAt, ImageParams imgParams, String imagePath);

    public void grabImages(IGrabImageListener listener, String videofilePath, int fromPoint, int everyMilliseconds,
                           int imageCount, ImageParams imgParams, String imageFolder, String imgFileNameFormat);

}

