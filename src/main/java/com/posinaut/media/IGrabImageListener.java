package com.posinaut.media;

public interface IGrabImageListener {
    public static final int STATUS_IMAGE_GRAB_COMPLETED = 1000;
    public static final int STATUS_IMAGE_GRAB_EXCEPTION = 2000;
    public static final int STATUS_IMAGE_GRAB_SDK_FAILED = 2010;
    public static final int STATUS_IMAGE_GRAB_SDK_NOT_SUPPORTED = 2020;

    public void onGrabImageComplete(int statusCode, String statusMessage, String[] imagesPath);

    public void onGrabImageProgress(int imageNo, String imagePath);
}

