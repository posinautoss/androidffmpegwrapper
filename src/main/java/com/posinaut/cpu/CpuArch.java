package com.posinaut.cpu;

public enum CpuArch {
    x86,
    ARMv7,
    ARMv7_NEON,
    NONE
}
