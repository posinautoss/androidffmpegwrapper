package com.posinaut.cpu;

import android.util.Log;

public class ArmArchHelper {
    private static final String Tag = ArmArchHelper.class.getSimpleName();

    static {
        // http://stackoverflow.com/questions/16519262/caused-by-java-lang-unsatisfiedlinkerror-cannot-load-library
        try {
            System.loadLibrary("ARM_ARCH");
        } catch (UnsatisfiedLinkError e) {
            Log.e(Tag, "Error loading native library 'ARM_ARCH'.", e);
        }
    }

    //
    // Native methods defined in c code must conform to the naming convention:
    //   Java_<package_space>_<class>_<method>
    //
    // So in our case the native method in armArch.c:
    //   Java_com_posinaut_cpu_ArmArchHelper_cpuArchFromJNI
    //
    native String cpuArchFromJNI();

    boolean isARM_v7_CPU(String cpuInfoString) {
        // Match against value from armArch.c
        return cpuInfoString.contains("v7");
    }

    boolean isNeonSupported(String cpuInfoString) {
        // Match against value from armArch.c
        return cpuInfoString.contains("neon");
    }
}
