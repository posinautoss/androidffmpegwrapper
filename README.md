[Android-FFmpeg-Wrapper](https://bitbucket.org/posinautoss/androidffmpegwrapper) 
====

An Android library for integrating FFmpeg functionality. The library contains: 

* Logic for detecting the ABI of the Android device it is run on (Armv7, ArmV7-Neon or x86).
* Logic for installing the packaged FFmpeg binaries onto the Android device.
* Wrapper code for executing commands against the FFmpeg binaries.


Notes
----
As of this writing, the code for installing the FFmpeg binaries (from apk to device) is always 
  to simply copy/replace regardless of whether the device version is idential to the apk version.

A better approach would be to perform a version check before doing so. One way you could probably do this
is to create a metadata version file (when the FFmpeg binaries are built) that contains the name of the binary and
its byte size. This is then packaged with the apk and on installation, also copied onto the device.

Then whenever an install check needs to be done, use the metadata version file in the apk and on the device
to determine whether to replace the device binaries or not.


Pre-Requisites
----

#####FFmpeg Binaries

You should have the FFmpeg binaries from [Android-FFmpeg-Build](https://bitbucket.org/posinautoss/androidffmpegbuild).

[1]For now you need *ffmpeg* and *ffprobe*.

Put the platform-specific binaries to the required folders in the */assets* folder:

* armeabi-v7a
* armeabi-v7a-neon
* x86

[1:] The initial commit of this repo contains the pre-built binaries.

#####Native Code

This library makes use of native code to determine the correct ARM platform if that is 
what is detected at runtime.

Gradle NDK building is not setup, so this is a manual process. 

Follow these steps:

a. Install the Android NDK and set it up on your path.

b. Open a terminal and navigate to the *jni* folder.

c. At the command line execute[2]:

````
	$ndk-build
````

d. The output of the build is placed in:

````
	/libs/armeabi/libARM_ARCH.so
	/libs/armeabi-v7a/libARM_ARCH.so
	/libs/x86/libARM_ARCH.so
````
    
e. Gradle includes the contents of the *libs* folder when creating the Android library.

[2:] *Application.mk* configures the target cpu platforms and *Android.mk* configures 
the build.


Library Integration
----

As this library is a stand-alone Git repository the best way to integrate it into 
another app that is its own Git repository is to use it as a Git sub module.

Here are the steps:

Set up your account on BitBucket for SSH authentication. See this 
[link](http://www.saintsatplay.com/blog/2014/03/setting-up-ssh-keys-on-mac-os-x-for-bitbucket#.VSQC-jvF-pU)
or this [one](https://confluence.atlassian.com/display/BITBUCKET/Set+up+SSH+for+Git)
if you don't have this setup.

Create the submodule in your local Git workspace that contains the Android app:

````
	// Add the library (tracking the master branch) to /submodules/ffmpeg-wrapper 
	$git submodule add -b master -f  https://bitbucket.org/RioDeJaneiro_Client/androidffmpegwrapper submodules/ffmpeg-wrapper
````

Verify the submodule's branch and the commit it is tracking.

````
	// http://stackoverflow.com/questions/12641469/list-submodules-in-a-git-repository
	$git submodule status --recursive
````

When you want to update the submodule...

````
	// http://stackoverflow.com/questions/9189575/git-submodule-tracking-latest/9189815#9189815
	$git submodule update --remote 
````

If you ever want to remove the sub module :-(

````
	// http://stackoverflow.com/questions/1260748/how-do-i-remove-a-git-submodule
	$git submodule deinit submodules/ffmpeg-wrapper    
	$git rm submodules/ffmpeg-wrapper
````


Library Usage
----

Use an implementation of *IFFmpeg* (for now *FFmpeg* is the only implementation) to load 
the FFmpeg binaries onto the device. You can provide a callback to listen to the status. 
On success, you can then use the *IFFmpeg* implementation to run commands against the 
supported binaries.

